#ifndef TPM_FUNC_H
#define TPM_FUNC_H

void tpm_func_start();
void tpm_func_close();
int tpm_func_pcr_read_write_file();
int tpm_func_pcr_extra(UINT32 uint32_pcr_index, char *pcr_extra_string);
void tpm_func_pcr_reset();

#endif
