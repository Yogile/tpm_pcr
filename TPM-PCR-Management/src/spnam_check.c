#define _XOPEN_SOURCE
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <pwd.h>
#include <shadow.h>
#include <unistd.h>

#include "spnam_check.h"

/**
 * 验证账户权限函数
 * 参数：char *name, char *passwd
 */
int spnam_check(char *name, char *passwd)
{
    struct passwd *struct_pw;
    struct_pw=getpwnam(name);
    if(struct_pw==NULL)
    {
        printf("\t -> [SPNAM] [info] struct_pw == NULL\n");
        return -1;
    }
    
    
    //getspnam是linux函数库中访问shadow的口令。
    //信息存贮在 struct spwd * 结构体之中
    struct spwd *struct_sp;
    struct_sp=getspnam(name);
    if(struct_sp == NULL)
    {
        printf("\t -> [SPNAM] [info] struct_sp == NULL\n");
        return -2;
    }
    
    //通过crypt函数校验密码：用输入的密码与密钥用相同的算法进行加密，返回新的密钥
    //Link with -lcrypt
    
    char *ret;
    //Link with -lcrypt
    ret=crypt(passwd,struct_sp->sp_pwdp);
    if(ret == NULL) {
        printf("\t -> [SPNAM] [info] ret == null\n");
        return -3;
    }

    if(strcmp(ret,struct_sp->sp_pwdp)==0)
    {
        printf("\t -> [SPNAM] [flag] [success] login.\n");
    }
    else
    {
        printf("\t -> [SPNAM] [flag] [failure] Strcmp Error: user not exist or passwd error.\n");
        return -4;
    }
    
    return 0;
}
