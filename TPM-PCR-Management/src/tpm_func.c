#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <tss/tss_error.h>
#include <tss/platform.h>
#include <tss/tss_defines.h>
#include <tss/tss_typedef.h>
#include <tss/tss_structs.h>
#include <tss/tspi.h>
#include <trousers/trousers.h>

#include "tpm_func.h"

#define Debug(message, tResult) printf("%s : %s\n", message, (char *)Trspi_Error_String(tpm_handle.result))

struct TPM_HANDLE {
    TSS_HCONTEXT hContext;
    TSS_HTPM hTPM;
    TSS_HPCRS hPcrComposite;
    TSS_RESULT result;

    UINT32 ulPcrIndex;
    UINT32 ulPcrValueLength;
    BYTE* rgbPcrValue;
    BYTE valueToExtend[250];
}tpm_handle;

/**
 * 获取 TPM、上下文对象、PCR合成对象 句柄
 * 参数：无
 */
void tpm_func_start()
{
    /* 告诉系统我们正在与本地TPM通讯 */
    tpm_handle.result = Tspi_Context_Create(&tpm_handle.hContext);
    Debug("\t -> [TPM_FUNC] Create &hContext Context", tpm_handle.result);
    
    tpm_handle.result = Tspi_Context_Connect(tpm_handle.hContext, NULL);
    Debug("\t -> [TPM_FUNC] Context hContext Connect", tpm_handle.result);
    
    /* 获得TPM句柄 */
    tpm_handle.result = Tspi_Context_GetTpmObject(tpm_handle.hContext, &tpm_handle.hTPM);
    Debug("\t -> [TPM_FUNC] Get TPM Handle", tpm_handle.result);
    
    
    tpm_handle.result = Tspi_Context_CreateObject(tpm_handle.hContext,TSS_OBJECT_TYPE_PCRS,TSS_PCRS_STRUCT_INFO,&tpm_handle.hPcrComposite);
    Debug("\t -> [TPM_FUNC] Create CreateObject hPcrComposite Context", tpm_handle.result);
}

/**
 * 释放 上下文对象 内存
 * 参数：无
 */
void tpm_func_close()
{
    printf("\t -> [TPM_FUNC] tpm_func_close\n");
    Tspi_Context_FreeMemory(tpm_handle.hContext, NULL);
    Tspi_Context_Close(tpm_handle.hContext);
}

/**
 * 读取 TPM PCR 的值，每个 PCR 值为一行写入 ./logs/pcr_read.txt 文件
 * 参数：无
 */
int tpm_func_pcr_read_write_file()
{
    FILE *fp;
    fp = fopen("./logs/pcr_read.txt", "w+");
    if(fp == NULL) {
        printf("\t -> [TPM_FUNC] tpm_func_pcr_read_write_file(): fp == NULL\n");
        return -1;
    }
    
    for (int j = 0; j < 24; j++)//24
    {
        tpm_handle.result = Tspi_TPM_PcrRead(tpm_handle.hTPM, j, &tpm_handle.ulPcrValueLength, &tpm_handle.rgbPcrValue);
        
        for (int i = 0; i < 20; i++) {
            //以 02x 的格式，写入文件
            fprintf(fp,"%02x",*(tpm_handle.rgbPcrValue + i));
        }
        //每行写入一个 \n 符号，方便读取
        fprintf(fp,"\n");
    }
    fclose(fp);

    return 0;
}

/**
 * 扩展指定的 PCR，再调用 tpm_func_pcr_read_write_file() 函数读取PCR、写入文件
 * 参数：
 * UINT32 uint32_pcr_index : 指定 PCR
 * char *pcr_extra_string : 扩展依据的内容
 * 返回值：int tpm_func_pcr_read_write_file()的返回值
 */
int tpm_func_pcr_extra(UINT32 uint32_pcr_index, char *pcr_extra_string)
{
    memset(tpm_handle.valueToExtend, 0, 250);
    memcpy(tpm_handle.valueToExtend, pcr_extra_string, strlen(pcr_extra_string));
    
    tpm_handle.result = Tspi_TPM_PcrExtend(tpm_handle.hTPM, uint32_pcr_index, 20, 
                                (BYTE *)tpm_handle.valueToExtend, NULL, &tpm_handle.ulPcrValueLength, &tpm_handle.rgbPcrValue);
	Debug("\t -> [TPM_FUNC] Extended the PCR", tpm_handle.result);
    
    int flag_tpm_func_pcr_read_write_file = tpm_func_pcr_read_write_file();
    
    return flag_tpm_func_pcr_read_write_file;
}


/**
 * 重置 TPM PCR
 * 参数：无
 */
void tpm_func_pcr_reset()
{
    tpm_handle.result = Tspi_TPM_PcrReset(tpm_handle.hTPM, tpm_handle.hPcrComposite);
    Debug("\t -> [TPM_FUNC] Create PcrReset Context", tpm_handle.result);
}


