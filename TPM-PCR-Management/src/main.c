#include <stdio.h>
#define _XOPEN_SOURCE
#include <unistd.h>
#include <shadow.h>
#include <pwd.h>

#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <tss/tss_error.h>
#include <tss/platform.h>
#include <tss/tss_defines.h>
#include <tss/tss_typedef.h>
#include <tss/tss_structs.h>
#include <tss/tspi.h>
#include <trousers/trousers.h>

#include <gtk/gtk.h>

#include "spnam_check.h"
#include "tpm_func.h"


#define Debug(message, tResult) printf("%s : %s\n", message, (char *)Trspi_Error_String(result))

char *pcr_result;

//主界面控件结构体
struct STRUCT_PCR_READ {
    GtkWidget *entry_pcr_read;
    GtkTextBuffer *buffer_pcr_read;
}struct_pcr_read;

//扩展 PCR 界面控件结构体 
struct STRUCT_PCR_EXTRA {
    int flag_win_extra_live;
    GtkWidget *win_extra;
    GtkWidget *fixed_box_extra;
    GtkWidget *entry_root_name;
    GtkWidget *entry_root_pw;
    GtkWidget *button_srk_check;
    GtkWidget *entry_pcr_index;
    GtkWidget *entry_pcr_extra_string;
    GtkWidget *button_pcr_info_check;
}struct_pcr_extra;

//提前声明 extra_pcr_win() ，避免隐式声明 warning
void extra_pcr_win(int flag_choose_win);

/**
 * 提示信息窗口函数
 * 用于弹出提示信息
 * 参数：char *win_title：窗口标题
 *      char *dialog_title：提示信息
 */
void dialog_message(const gchar *win_title, const gchar *dialog_title)
{
    printf("[Dialog]\tUse dialog_message!\n");
    GtkWidget *dialog;
    dialog = gtk_message_dialog_new(struct_pcr_extra.win_extra,
                GTK_DIALOG_DESTROY_WITH_PARENT,
                GTK_MESSAGE_INFO,
                GTK_BUTTONS_OK,
                dialog_title, "title");
    gtk_window_set_title(GTK_WINDOW(dialog), win_title);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}


/**
 * 读取 PCR LOG 文件内容并显示在多行文本框函数
 * 调用 tpm_func.c 中的 tpm_func_pcr_read_write_file() 函数，读取 TPM PCR 并写入文件
 * 用于 struct_pcr_read.buffer_pcr_read 的 GtkWidget *button 的回调函数
 * 参数：GtkWidget *button, gpointer data
 */
static void get_pcr_value(GtkWidget *button , gpointer data)
{
    printf("\n[MAIN]\t[START]\tget_pcr_value()\n");
    //调用 tpm_func.c 中的 tpm_func_pcr_read_write_file() 函数，读取 TPM PCR 并写入文件
    int flag_tpm_func_pcr_read_write_file = tpm_func_pcr_read_write_file();

    if(flag_tpm_func_pcr_read_write_file == -1) {
        dialog_message("File Prompt", "tpm_func_pcr_read_write_file fp == NULL，文件打开错误。");
        return;
    }

    FILE *fp;
    char pcr_value[24][41];
    int i;
    fp = fopen ("./logs/pcr_read.txt", "r");

    if(fp == NULL) {
        dialog_message("File Prompt", "fp == NULL，文件打开错误。");
        printf("\t[info]\tget_pcr_value fp == NULL\n");
        return;
    }
    rewind(fp);
    for(i=0;i<24;i++) {
        fscanf(fp, "%s", pcr_value[i]);
    }

    fclose(fp);

    char temp_add[1000];    //先分配一块足够的空间

    strcpy(temp_add, pcr_value[0]);   //然后把p复制进去
    for(i=1;i<24;i++) {
        strcat(temp_add, "\n");
        strcat(temp_add, pcr_value[i]);   //再把a添加到后面
    }
    pcr_result=temp_add;

    //为多行文本框控件分配缓冲区
    struct_pcr_read.buffer_pcr_read = gtk_text_view_get_buffer(GTK_TEXT_VIEW(struct_pcr_read.entry_pcr_read));
    //从头显示缓冲区字符
    gtk_text_buffer_set_text (struct_pcr_read.buffer_pcr_read, pcr_result, -1);
    
    printf("[MAIN]\t[END]\tget_pcr_value()\n");
}


/**
 * PCR扩展的 root 账户验证函数
 * 用于 extra_pcr_win(0) 的 GtkWidget *button 的会调函数
 * 参数：GtkWidget *button, gpointer data
 */
static void extra_root_check(GtkWidget *button , gpointer data)
{
    printf("\n[MAIN]\t[START]\textra_root_check()\n");
    
    const gchar *entry_root_name_text;
    const gchar *entry_root_pw_text; 
	// 获得文本内容
	entry_root_name_text = gtk_entry_get_text(GTK_ENTRY(struct_pcr_extra.entry_root_name));
    entry_root_pw_text = gtk_entry_get_text(GTK_ENTRY(struct_pcr_extra.entry_root_pw));

    char *name;
    char *passwd;
    
    name = (char *)entry_root_name_text;
    passwd = (char *)entry_root_pw_text;
    
    //调用 spnam_check.c 中的 spnam_check() 函数，通过返回值判断认证成功与否，以及错误类型
    int flag_root_check = spnam_check(name, passwd);
    if(flag_root_check == 0) {
        printf("\t[flag]\t[success]\textra_root_check() : spnam_check().\n");
    }
    else if(flag_root_check == -1) {
        dialog_message("Prompt", "Root struct passwd 账户名不存在，请重新输入。");
        return;
    }
    else if(flag_root_check == -2) {
        dialog_message("Prompt", "没有权限，请使用 root 权限运行本程序。");
        //"Root struct spwd 账户名不存在，请重新输入。"
        return;
    }
    else if(flag_root_check == -3) {
        dialog_message("Prompt", "Crypt 函数错误，请检查。");
        return;
    }
    else if(flag_root_check == -4) {
        dialog_message("Prompt", "账户名或密码输入错误。");
        return;
    }
    
    printf("\t[flag]\t[success]\tRoot check!\n");
    
    //销毁PCR扩展认证窗口
    gtk_widget_destroy(struct_pcr_extra.win_extra);
    //设定PCR扩展窗口标志为0，已消除
    struct_pcr_extra.flag_win_extra_live = 0;
    //打开PCR值扩展窗口
    extra_pcr_win(1);
    
    printf("[MAIN]\t[END]\textra_root_check()\n");
}

/**
 * PCR扩展信息检查并扩展函数
 * 用于 extra_pcr_win(1) 的 GtkWidget *button 的会调函数
 * 检查符合规定后，扩展
 * 参数：GtkWidget *button, gpointer data
 */
static void extra_pcr_info_check(GtkWidget *button , gpointer data)
{
    printf("\n[MAIN]\t[START]\textra_pcr_info_check()\n");
    
    int i;
    const gchar *entry_pcr_index_text;
    const gchar *entry_pcr_extra_string_text;
	// 获得文本内容
	entry_pcr_index_text = gtk_entry_get_text(GTK_ENTRY(struct_pcr_extra.entry_pcr_index));
    entry_pcr_extra_string_text = gtk_entry_get_text(GTK_ENTRY(struct_pcr_extra.entry_pcr_extra_string));
    
    int strlen_pcr_index = strlen(entry_pcr_index_text);
    int strlen_pcr_extra_string = strlen(entry_pcr_extra_string_text);
    
    UINT32 uint32_pcr_index;
    
    //判断pcr_index长度，以及pcr_index数字
    switch(strlen_pcr_index){
        case 1:
            printf("\t[info]\tindex: %d\n", strlen_pcr_index);
            if(*entry_pcr_index_text < '0' || *entry_pcr_index_text > '9') {
                dialog_message("PCR Index Error","PCR Index 不是一或两位 0-9 的数字。");
                return;
            }
            break;
        case 2:
            printf("[info] index: %d\n", strlen_pcr_index);
            if(*entry_pcr_index_text < '0' || *entry_pcr_index_text > '9') {
                dialog_message("PCR Index Error","PCR Index 不是一或两位 0-9 的数字。");
                return;
            }
            if(*(entry_pcr_index_text+1) < '0' || *(entry_pcr_index_text+1) > '9') {
                dialog_message("PCR Index Error","PCR Index 不是一或两位 0-9 的数字。");
                return;
            }
            break;
        default:
            printf("\t[info]\tstrlen_pcr_index: %d\n", strlen_pcr_index);
            dialog_message("PCR Index Error","PCR Index 为空或者超出范围。");
            return;
    }
    
    //将满足条件的index转为Uint32类型
    uint32_pcr_index = atoi(entry_pcr_index_text);
    printf("\t[info]\tuint32_pcr_index: %d\n", uint32_pcr_index);
    
    //将2位数的pcr_index缩小范围
    if(uint32_pcr_index < 0 || uint32_pcr_index > 23) {
        dialog_message("PCR Index Error","PCR Index 为空或者超出范围。");
        return;
    }

    //判断pcr_extra_string字符长度，else判断是否为数字、小写字母或小写字母
    if(strlen_pcr_extra_string <= 0 || strlen_pcr_extra_string > 40) {
        dialog_message("PCR Extra Error","PCR Extra 长度不符合 40 个字符限制，至少输入一个字符。");
        return;
    }
    else {
        printf("\t[info]\tpcr_extra_string i_for_value == ");
        for(i=0; i<strlen_pcr_extra_string; i++) {
            if( (*(entry_pcr_extra_string_text+i) >= '0' && *(entry_pcr_extra_string_text+i) <= '9') 
                || (*(entry_pcr_extra_string_text+i) >= 'a' && *(entry_pcr_extra_string_text+i) <= 'z') 
                || (*(entry_pcr_extra_string_text+i) >= 'A' && *(entry_pcr_extra_string_text+i) <= 'Z') )
            {
                continue;
            }
            else {
                printf("%d\n", i);
                dialog_message("PCR Extra Error","PCR Extra 包含范围之外的字符。");
                return;
            }
        }
        printf("%d\n", i);
        
        printf("\t[flag]\t[success]\tCheck entry_pcr_extra_string.\n");
    }
    
    //调用 tpm_func.c 中的 tpm_func_pcr_extra() 函数
    int flag_tpm_func_pcr_extra = tpm_func_pcr_extra(uint32_pcr_index, (char *)entry_pcr_extra_string_text);
    
    printf("[MAIN]\t[END]\textra_pcr_info_check()\n");
}

/**
 * PCR扩展的窗口关闭函数
 * 关闭PCR扩展的相关窗口，设定窗口 struct_pcr_extra.flag_win_extra_live 值为 0
 * 参数：无
 */
static void extra_pcr_win_destroy(GtkWidget *button , gpointer data)
{
    printf("\n[MAIN]\t[START]\textra_pcr_win_destroy()\n");
    //销毁PCR扩展窗口
    gtk_widget_destroy(struct_pcr_extra.win_extra);
    //设定PCR扩展窗口标志为0，已消除
    struct_pcr_extra.flag_win_extra_live = 0;
    
    printf("[MAIN]\t[END]\textra_pcr_win_destroy()\n");
}


/**
 * PCR扩展的窗口函数
 * 打开PCR扩展的相关窗口
 * 参数：int flag_choose_win：控制PCR扩展的窗口参数
 * flag_choose_win=0，root登陆窗口
 * flag_choose_win=1，pcr扩展窗口
 */
void extra_pcr_win(int flag_choose_win)
{
    printf("\n[MAIN]\t[START]\textra_pcr_win()\n");
    
    if(struct_pcr_extra.flag_win_extra_live == 1) {
        dialog_message("Prompt", "扩展 PCR 窗口已打开。");
        return;
    }
    //设定PCR扩展窗口标志为1，已存在
    struct_pcr_extra.flag_win_extra_live = 1;
    
    //判断控制PCR扩展的窗口参数
    if(flag_choose_win == 0)
    {
        printf("\t[info]\tflag_choose_win == 0\n");
        
        //创建一个窗口
        struct_pcr_extra.win_extra = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        //设置窗口的标题
        gtk_window_set_title(GTK_WINDOW(struct_pcr_extra.win_extra) , "扩展 PCR 身份验证");
        //设置窗口默认大小
        gtk_window_set_default_size(GTK_WINDOW(struct_pcr_extra.win_extra) , 500 , 270);
        //创建一个固定容器
        struct_pcr_extra.fixed_box_extra = gtk_fixed_new();
        //将容器包含进window中
        gtk_container_add(GTK_CONTAINER(struct_pcr_extra.win_extra) , struct_pcr_extra.fixed_box_extra);
        
        
        GtkWidget *label_title = gtk_label_new("身份验证");
        gtk_fixed_put(GTK_FIXED(struct_pcr_extra.fixed_box_extra), label_title, 220, 20);
        
        GtkWidget *label_root_name = gtk_label_new("输入当前 Linux 登陆账户名：");
        gtk_fixed_put(GTK_FIXED(struct_pcr_extra.fixed_box_extra), label_root_name, 50, 70);
        
        //GtkWidget *entry_root_name = gtk_entry_new();  // 创建行编辑 	
        struct_pcr_extra.entry_root_name = gtk_entry_new();  // 创建行编辑 	
        //按钮添加到固定布局
        gtk_fixed_put(GTK_FIXED(struct_pcr_extra.fixed_box_extra), struct_pcr_extra.entry_root_name, 260, 70);
        gtk_entry_set_max_length(GTK_ENTRY(struct_pcr_extra.entry_root_name), 30);     // 设置行编辑显示最大字符的长度
        gtk_entry_set_text(GTK_ENTRY(struct_pcr_extra.entry_root_name), "");  // 设置内容
        
        GtkWidget *label_root_pw = gtk_label_new("输入当前 Linux 登陆密码：");
        gtk_fixed_put(GTK_FIXED(struct_pcr_extra.fixed_box_extra), label_root_pw, 50, 130);
        
        //GtkWidget *entry_root_pw = gtk_entry_new();  // 创建行编辑 	
        struct_pcr_extra.entry_root_pw = gtk_entry_new();  // 创建行编辑 	
        //按钮添加到固定布局
        gtk_fixed_put(GTK_FIXED(struct_pcr_extra.fixed_box_extra), struct_pcr_extra.entry_root_pw, 260, 130);
        gtk_entry_set_max_length(GTK_ENTRY(struct_pcr_extra.entry_root_pw), 30);     // 设置行编辑显示最大字符的长度
        gtk_entry_set_text(GTK_ENTRY(struct_pcr_extra.entry_root_pw), "");  // 设置内容
        gtk_entry_set_visibility(GTK_ENTRY(struct_pcr_extra.entry_root_pw), FALSE); 	 // 密码模式
        
        
        //创建一个按钮，并带有“EXTRA PCR” 的标签
        struct_pcr_extra.button_srk_check = gtk_button_new_with_label("验证");
        //按钮添加到固定布局
        gtk_fixed_put(GTK_FIXED(struct_pcr_extra.fixed_box_extra), struct_pcr_extra.button_srk_check, 200, 190);
        //设置控件的大小
        gtk_widget_set_size_request(struct_pcr_extra.button_srk_check, 100, 50);
        //连接信号，让点击按钮后，便调用 extra_root_check 函数
        g_signal_connect(struct_pcr_extra.button_srk_check , "clicked" , G_CALLBACK( extra_root_check ) , GINT_TO_POINTER(4));
        
        //显示window及其所有控件
        gtk_widget_show_all(struct_pcr_extra.win_extra);
    
    }
    else if(flag_choose_win == 1)
    {
        printf("\t[info]\tflag_choose_win == 1\n");
        //创建一个窗口
        struct_pcr_extra.win_extra = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        //设置窗口的标题
        gtk_window_set_title(GTK_WINDOW(struct_pcr_extra.win_extra) , "扩展 PCR");
        //设置窗口默认大小
        gtk_window_set_default_size(GTK_WINDOW(struct_pcr_extra.win_extra) , 450 , 300);
        //创建一个固定容器
        struct_pcr_extra.fixed_box_extra = gtk_fixed_new();
        //将容器包含进window中
        gtk_container_add(GTK_CONTAINER(struct_pcr_extra.win_extra) , struct_pcr_extra.fixed_box_extra);
        
        //声明标签
        GtkWidget *label_title = gtk_label_new("扩展 PCR 的 Index(0-23)");
        gtk_fixed_put(GTK_FIXED(struct_pcr_extra.fixed_box_extra), label_title, 30, 30);
        
        //GtkWidget *entry_pcr_index = gtk_entry_new();  // 创建行编辑 	
        struct_pcr_extra.entry_pcr_index = gtk_entry_new();  // 创建行编辑 	
        //按钮添加到固定布局
        gtk_fixed_put(GTK_FIXED(struct_pcr_extra.fixed_box_extra), struct_pcr_extra.entry_pcr_index, 50, 70);
        gtk_widget_set_size_request(struct_pcr_extra.entry_pcr_index, 350, 10);
        gtk_entry_set_max_length(GTK_ENTRY(struct_pcr_extra.entry_pcr_index), 20);     // 设置行编辑显示最大字符的长度
        gtk_entry_set_text(GTK_ENTRY(struct_pcr_extra.entry_pcr_index), "");  // 设置内容
        
        //声明标签
        GtkWidget *label_one = gtk_label_new("扩展 PCR 的内容(40位数字、大写或小写字母)");
        gtk_fixed_put(GTK_FIXED(struct_pcr_extra.fixed_box_extra), label_one, 30, 120);
        
        //GtkWidget *entry_pcr_extra_string = gtk_entry_new();  // 创建行编辑 	
        struct_pcr_extra.entry_pcr_extra_string = gtk_entry_new();  // 创建行编辑 	
        gtk_fixed_put(GTK_FIXED(struct_pcr_extra.fixed_box_extra), struct_pcr_extra.entry_pcr_extra_string, 50, 160);
        gtk_widget_set_size_request(struct_pcr_extra.entry_pcr_extra_string, 350, 10);
        gtk_entry_set_max_length(GTK_ENTRY(struct_pcr_extra.entry_pcr_extra_string), 40);     // 设置行编辑显示最大字符的长度
        gtk_entry_set_text(GTK_ENTRY(struct_pcr_extra.entry_pcr_extra_string), "");  // 设置内容
        
        
        //创建一个按钮，并带有“验证” 的标签
        struct_pcr_extra.button_pcr_info_check = gtk_button_new_with_label("扩展");
        //按钮添加到固定布局
        gtk_fixed_put(GTK_FIXED(struct_pcr_extra.fixed_box_extra), struct_pcr_extra.button_pcr_info_check, 175, 220);
        //设置控件的大小
        gtk_widget_set_size_request(struct_pcr_extra.button_pcr_info_check, 100, 50);
        //连接信号，让点击按钮后，便调用 extra_pcr_info_check 函数
        g_signal_connect(struct_pcr_extra.button_pcr_info_check , "clicked" , G_CALLBACK( extra_pcr_info_check ) , GINT_TO_POINTER(4));
        
        
        //显示window及其所有控件
        gtk_widget_show_all(struct_pcr_extra.win_extra);
    }
    
    //连接信号，让点击右上角的 X 关闭按钮后，便调用 extra_pcr_win_destroy 函数
    g_signal_connect(struct_pcr_extra.win_extra,"delete_event",G_CALLBACK( extra_pcr_win_destroy ),NULL);
    
    printf("[MAIN]\t[END]\textra_pcr_win()\n");
}

/**
 * PCR扩展启用函数
 * 用于主窗口 扩展按钮 GtkWidget *button 的会调函数
 * 调用extra_pcr_win(int flag_choose_win) flag_choose_win=0
 * 参数：GtkWidget *button, gpointer data
 */
static void extra_pcr(GtkWidget *button , gpointer data)
{
    printf("\n[MAIN]\t[START]\textra_pcr()\n");
    
    extra_pcr_win(0);
    
    printf("[MAIN]\t[END]\textra_pcr()\n");
}

/**
 * 控件函数
 */
static void controller(GtkWidget *fixed_box)
{
    printf("\n[MAIN]\t[START]\tcontroller()\n");
    
    //申明一个按钮
    GtkWidget *button_pcr_get;
    //申明一个按钮
    GtkWidget *button_pcr_extra;
    
    //创建一个按钮，并带有“GET PCR” 的标签
    button_pcr_get = gtk_button_new_with_label("读取 PCR");
    //按钮添加到固定布局
    gtk_fixed_put(GTK_FIXED(fixed_box), button_pcr_get, 100, 50);
    //设置控件的大小
    gtk_widget_set_size_request(button_pcr_get, 100, 50);
    //连接信号，让点击按钮后，便调用 get_pcr_value 函数
    g_signal_connect(button_pcr_get , "clicked" , G_CALLBACK( get_pcr_value ) , NULL);
    

    //创建一个按钮，并带有“EXTRA PCR” 的标签
    button_pcr_extra = gtk_button_new_with_label("扩展 PCR");
    //按钮添加到固定布局
    gtk_fixed_put(GTK_FIXED(fixed_box), button_pcr_extra, 300, 50);
    //设置控件的大小
    gtk_widget_set_size_request(button_pcr_extra, 100, 50);
    //连接信号，让点击按钮后，便调用 extra_pcr 函数
    g_signal_connect(button_pcr_extra , "clicked" , G_CALLBACK( extra_pcr ) , NULL);
    
    
    //创建一个文本框
    struct_pcr_read.entry_pcr_read = gtk_text_view_new();
    //文本框添加到固定布局
    gtk_fixed_put(GTK_FIXED(fixed_box), struct_pcr_read.entry_pcr_read, 10, 110);
    //设置控件的大小
    gtk_widget_set_size_request(struct_pcr_read.entry_pcr_read, 480, 410);
    
    printf("[MAIN]\t[END]\tcontroller()\n");
}

/**
 * 容器函数
 */
static void activate(GtkApplication *app , gpointer data)
{
    printf("\n[MAIN]\t[START]\tactivate()\n");
    
    //申明一个窗口
    GtkWidget *win;

    //申明一个盒容器，用于容纳按钮，控制大小
    GtkWidget *fixed_box;

    //为app创建一个窗口
    win = gtk_application_window_new(app);

    //设置窗口的标题
    gtk_window_set_title(GTK_WINDOW(win) , "TPM平台配置寄存器PCR管理程序");
    
    //设置窗口默认大小为长宽各200像素
    gtk_window_set_default_size(GTK_WINDOW(win) , 500 , 550);

    //创建一个固定容器
    fixed_box = gtk_fixed_new();
    
    //将容器包含进window中
    gtk_container_add(GTK_CONTAINER(win) , fixed_box);

    //调用控件函数
    controller(fixed_box);
        
    //显示window及其所有控件
    gtk_widget_show_all(win);
    
    printf("[MAIN]\t[END]\tactivate()\n");
}

int main(int argc , char **argv)
{
    printf("\n[MAIN]\t[START]\tmain()\n");
    
    //初始化 TPM 相关句柄
    tpm_func_start();
    
    //设定PCR扩展窗口标志为0，未创建
    struct_pcr_extra.flag_win_extra_live = 0;
    
    //申明创建一个 GtkApplicatin对象名为app
    GtkApplication *app;
    
    //用于拿到app 运行结束后的返回值
    int app_status;
    
    //创建一个application
    app = gtk_application_new("org.gtk.exmple" , G_APPLICATION_FLAGS_NONE);
    
    //连接信号，初始化app时，调用 activate 函数
    g_signal_connect(app , "activate" , G_CALLBACK(activate) , NULL);

    //运行app
    app_status = g_application_run(G_APPLICATION(app) , argc , argv);

    //销毁app
    g_object_unref(app);
    
    tpm_func_close();

    
    printf("[MAIN]\t[END]\tmain()\n");
    
    return app_status;
}
