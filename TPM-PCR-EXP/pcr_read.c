#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <tss/tss_error.h>
#include <tss/platform.h>
#include <tss/tss_defines.h>
#include <tss/tss_typedef.h>
#include <tss/tss_structs.h>
#include <tss/tspi.h>
#include <trousers/trousers.h>  

#define Debug(message, tResult) printf("%s : %s\n", message, (char *)Trspi_Error_String(result))


void pcr_read() {
    // 声明上下文对象句柄
    TSS_HCONTEXT hContext = 0;
    TSS_HTPM hTPM = 0;
    TSS_RESULT result;

    UINT32 ulPcrIndex;
    UINT32 pulPcrValueLength;
    BYTE* prgbPcrValue;
    
    /* 告诉系统我们正在与本地TPM通讯 */
    result = Tspi_Context_Create(&hContext);
    Debug("Create &hContext Context", result);
    
    result = Tspi_Context_Connect(hContext, NULL);
    Debug("Context hContext Connect", result);
    
    /* 获得TPM句柄 */
    result = Tspi_Context_GetTpmObject(hContext, &hTPM);
    Debug("Get TPM Handle", result);

    //输出所有PCR寄存器内的值
    /*********************/
    for (int j = 0; j < 2; j++)//24
    {
        result = Tspi_TPM_PcrRead(hTPM, j, &pulPcrValueLength, &prgbPcrValue);
        printf("PCR %02d ", j);
        for (int i = 0; i < 20; i++) {
            printf("%02x", *(prgbPcrValue + i));
        }
        printf("\n");
        
        
    }
    
    //Clean up
    Tspi_Context_FreeMemory(hContext, NULL);
    Tspi_Context_Close(hContext);
}

int main()
{
	pcr_read();
	return 0;
}
